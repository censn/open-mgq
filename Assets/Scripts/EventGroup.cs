﻿using System.Collections;

public class EventGroup : IEvent {
    private IEvent[] events;

    public EventGroup(params IEvent[] events) {
        this.events = events;
    }

    public IEnumerator Play() {
        foreach (IEvent e in events) {
            yield return e.Play();
        }
    }
}
﻿using System;
using UnityEngine;

public class SpriteList : MonoBehaviour {
    [Serializable]
    public class Ilias {
        public Sprite normal;
        public Sprite angry;
        public Sprite smile;
    }
    [Serializable]
    public class Villagers {
        public Sprite tallBald;
        public Sprite tallHair;
        public Sprite betty;
    }

    public Ilias ilias;
    public Villagers villagers;
}
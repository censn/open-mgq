﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundList : MonoBehaviour {
    public Sprite heaven;
    public Sprite home;
    public Sprite town;
    public Sprite woods;
}
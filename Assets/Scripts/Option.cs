﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Option : MonoBehaviour {
    [SerializeField]
    private Button button;

    [SerializeField]
    private TextMeshProUGUI text;

    public void Init(Transform parent, string text, Action action, bool isEnabled = true) {
        this.text.text = text;
        this.transform.SetParent(parent);
        button.onClick.AddListener(new UnityAction(action));
        button.interactable = isEnabled;
    }
}

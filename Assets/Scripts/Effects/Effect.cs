﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Effect {
    public AudioClip clip;
    public SpriteRenderer sprite;

    public IEnumerator Play(Transform parent) {
        SoundManager.Instance.Play(clip);

        if (sprite != null) {
            SpriteRenderer sr = GameObject.Instantiate(sprite, parent);
            sr.sortingOrder = 1;
            yield return new WaitWhile(() => sr != null);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectManager : MonoBehaviour {

    private static EffectManager _instance;

    [SerializeField]
    private Transform parent;

    [SerializeField]
    private Transform canvas;

    [SerializeField]
    private Effect attack;

    [SerializeField]
    private Effect decapitation;

    [SerializeField]
    private Effect shake;

    [SerializeField]
    private AudioClip thunder;

    [SerializeField]
    private Image frontground;

    public static EffectManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<EffectManager>();
            }
            return _instance;
        }
    }

    public IEnumerator PlayAttack() {
        yield return attack.Play(parent);
    }

    public IEnumerator PlayDecapitation() {
        yield return decapitation.Play(parent);
    }

    public IEnumerator PlayScreenShake() {
        yield return shake.Play(parent);
        yield return Util.Shake(15, 0, 0.10f, canvas);
    }

    public IEnumerator PlayLightning() {
        frontground.color = Color.white;
        SoundManager.Instance.Play(thunder);
        yield return Util.Lerp(0.05f, t => Util.SetAlpha(frontground, t));
        yield return Util.Lerp(0.25f, t => Util.SetAlpha(frontground, 1 - t));
        yield return Util.Shake(30, .1f, 0.10f, canvas);
    }
}

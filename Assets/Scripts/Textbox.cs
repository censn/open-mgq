﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class Textbox : MonoBehaviour {
    private const int CURSOR_X_OFFSET = 18;

    [SerializeField]
    private new TextMeshProUGUI name;

    [SerializeField]
    private TextMeshProUGUI text;

    [SerializeField]
    private SpriteRenderer endOfLine;

    [SerializeField]
    private SpriteRenderer endOfPage;

    public void SetName(string target) {
        name.SetText(target);
    }

    public IEnumerator Write(string target) {
        HashSet<int> specialIndices = new HashSet<int>(Enumerable.Range(0, target.Length).Where(index => target[index] == '/'));
        target = target.Replace("/", "\n");
        text.SetText(target);
        text.ForceMeshUpdate();
        if (string.IsNullOrEmpty(target)) {
            yield break;
        }
        TMP_TextInfo info = text.textInfo;
        for (int i = 1; i < info.pageCount + 1; i++) { // 1 indexed whyyy
            int pageStartIndex = info.pageInfo[i - 1].firstCharacterIndex;
            int pageEndIndex = info.pageInfo[i - 1].lastCharacterIndex;
            text.pageToDisplay = i;
            for (int j = pageStartIndex; j <= pageEndIndex; j++) {
                text.maxVisibleCharacters = j + 1;
                if (specialIndices.Contains(j)) { // increase to offset Replaces
                    endOfLine.enabled = true;
                    SetCursorPosition(endOfLine.transform, info, j);
                    yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Mouse0));
                    endOfLine.enabled = false;
                }
                yield return new WaitForSeconds(0.001f);
            }
            text.ForceMeshUpdate();
            info = text.textInfo;
            endOfPage.enabled = true;
            SetCursorPosition(endOfPage.transform, info, pageEndIndex);
            yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Mouse0));
            endOfPage.enabled = false;
            text.SetText(string.Empty);
        }
    }

    private void SetCursorPosition(Transform cursor, TMP_TextInfo info, int characterIndex) {
        TMP_CharacterInfo lastCharInLine = info.characterInfo[characterIndex];
        Vector3 baselineEnd = new Vector3(lastCharInLine.topRight.x, 
            text.transform.TransformPoint(new Vector3(0, lastCharInLine.baseLine, 0)).y, 0);
        float width = Mathf.Abs(lastCharInLine.bottomLeft.x - lastCharInLine.bottomRight.x);
        cursor.transform.position = baselineEnd + new Vector3(Mathf.Max(CURSOR_X_OFFSET, width),0, 0);
    }
        
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    private static GameManager _instance;

    [SerializeField]
    private SpriteList c;

    [SerializeField]
    private MusicList m;

    [SerializeField]
    private SoundList s;

    [SerializeField]
    private BackgroundList b;

    [SerializeField]
    private EnemyList en;

    [SerializeField]
    private BattleManager battle;

    [SerializeField]
    private Enemy test;

    [SerializeField]
    private EventManager e;

    private const string LUKA = "Luka";
    private const string ILIAS = "Ilias";

    public static GameManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    private Event E {
        get {
            return new Event();
        }
    }

    private void Start() {
        Play(
            e.E.SetBackground(b.heaven),
            e.Write("Where am I..."),
            e.Write("A soft light fills the area, giving the atmosphere a solemn feel."),
            e.Write("...Is this a dream?"),
            e.Write("Luka..."),
            e.Write("Oh brave Luka..."),
            e.Write("I hear a beautiful voice begin to call out to me."),
            e.Write("The goddess Ilias suddenly appears before me!")
                .AddSprite(c.ilias.normal, true)
                .SetMusic(m.ilias),
            e.Write("Ilias", "Oh brave Luka... can you hear my voice?"),
            e.E.AddChoices(
                new Choice("I can!", iCan()),
                new Choice("Nope", nope()),
                new Choice("....", dots())
                )
            );
    }

    private EventGroup iCan() {
        return new EventGroup(
            e.Write(LUKA, "I can hear you, Ilias!"),
            e.Write("I begin to tremble at the sound of Ilias' voice./The goddess whom created the world, who extends her love to Humanity."),
            e.Write("With such an amazing figure appearing before me, it makes me want to dance!/Even if it's just a dream..."),
            normal()
        );
    }

    private EventGroup nope() {
        return new EventGroup(
            e.Write(ILIAS, "............"),
            e.Write(ILIAS, "Then why did you respond?"),
            e.Write(LUKA, "............"),
            e.Write("She got me there..."),
            normal()
        );
    }

    private EventGroup dots() {
        return new EventGroup(
            e.Write(ILIAS, "............"),
            e.Write(LUKA, "............"),
            e.Write(ILIAS, "If you ignore me, I shall pass judgement on you."),
            e.Write(ILIAS, "Do you understand?"),
            e.E.AddChoices(
                new Choice("Sorry", normal()),
                new Choice("......", moreDots()))
        );
    }

    private EventGroup moreDots() {
        return new EventGroup(
            e.E.DeleteSprite(c.ilias.normal)
               .AddSprite(c.ilias.angry),
            e.Write(ILIAS, "......"),
            e.Write(ILIAS, "Die."),
            e.E.SetMusic(m.ilias2),
            e.E.StartBattle(en.ilias, new EventGroup(
                e.E.AddChoices(
                    new Choice("Retry", null),
                    new Choice("Title", null)
                )
            ))
            );
    }

    private EventGroup normal() {
        return new EventGroup(
                e.Write(ILIAS, "Many, many years ago, in a time man cannot comprehend, I created this world."),
                e.Write(ILIAS, "First was the earth, sky, and sea./Then the animals, birds and insects./Finally, I created Humanity."),
                e.Write(ILIAS, "However, I am not perfect."),
                e.Write(ILIAS, "While creating Humanity, I also had many failures..."),
                e.Write(ILIAS, "Those failures are monsters... a truly detestable existence."),
                e.Write(LUKA, "..........."),
                e.Write(ILIAS, "Monsters are nothing but evil."),
                e.Write(ILIAS, "They will seduce humans to commit forbidden acts. Sometimes even committing great acts of violence."),
                e.Write(ILIAS, "Even though humans are weak, I still love them.../So I hate these monsters that bring them only harm."),
                e.Write(ILIAS, "And you, Luka, have finally come of age today, have you not?"),
                e.Write(LUKA, "Yes... I have long been looking forward to this day!"),
                e.Write("Today, I shall finally receive the baptism in Ilias's name./After being baptized, I will be recognized as a Hero!"),
                e.Write("Being a Hero has always been my dream, and finally the long awaited day has arrived!/Even though my village has been peaceful, I devoted all of my time to training with a sword."),
                e.Write("And finally!/The day when I shall finally become a Hero has come at last!"),
                e.Write(ILIAS, "Up until now, I have given my blessing and protection to many men.../However, the monsters still have not been exterminated."),
                e.Write(ILIAS, "Not since Heinrich, 500 years ago, has a man been able to defeat a Monster Lord."),
                e.Write("The hero Heinrich.../500 Years ago, he was able to defeat a horribly cruel Monster Lord./With an attack that split the earth itself, he was able to slay the evil being."),
                e.Write("Truly, a Hero among Heroes."),
                e.Write(ILIAS, "But Luka.../You, too, have the potential to be able to defeat the Monster Lord!")
                    .DeleteSprite(c.ilias.normal)
                    .AddSprite(c.ilias.smile),
                e.Write(LUKA, "...Eh?/I... I do?"),
                e.Write(ILIAS, "Now go, Luka!/I will always be watching over you..."),
                e.E.AddTransition(),
                e.E.SetBackground(b.home)
                    .SetMusic(m.calm)
                    .DeleteSprite(c.ilias.smile),
                e.Write("I open my eyes to the soft light of the morning sun coming through the window."),
                e.Write("Was that just a dream...?/...There's no way that was just a normal dream!"),
                e.Write("I definitely talked to Ilias!"),
                e.Write(LUKA, "Oh great Ilias... thank you!/Please watch over me!"),
                e.Write(LUKA, "Like every day, I start off with a prayer to Ilias."),
                e.Write("After my prayers have finished, I turn to the keepsake of my mother."),
                e.Write(LUKA, "Good morning, Mother./Today I will finally begin my journey as a Hero."),
                e.Write("With my morning routine finished, I begin to get ready for my trip./It's a beautiful day outside, with the fresh morning scent filling the air."),
                e.Write("This morning I will travel to Ilias Temple to receive my baptism./Once baptized, I will begin my journey to defeat the Monster Lord!"),
                e.Write("...I won't be returning to this house for a while./With such a sad thought in my mind, I look around my small home."),
                e.Write("Luka", "Well, until I defeat the Monster Lord, I won't be coming back here.../I better clean you so you look great when I come back!"),
                e.Write("I start off making my bed.")
                    .AddSound(s.steps),
                e.Write("Villager", "He... Help!"),
                e.Write(LUKA, "Hmm? What was that?"),
                e.Write("While making my bed, I hear a man's scream./It sounded like Hans, the lumberjack./What's happening so early in the morning...?"),
                e.Write("Hans", "Monster... A monster is in the forest!"),
                e.Write(LUKA, "W... What!?")
                    .SetMusic(m.crisis),
                e.Write("In Ilias Village? How can a monster appear in such a peaceful village...?"),
                e.Write("Even though Ilias Village is small, it still has a gigantic temple./The temple where the goddess is revered: Ilias Temple./With such a huge temple nearby, monsters dare not approach this village."),
                e.Write("Villager", "Everyone, hide in your houses! Quickly, before the monster comes into the village!"),
                e.Write("Woman", "Ahhhh!"),
                e.Write("Child", "Mommy!"),
                e.Write("Such a peaceful village has quickly fallen into panic./On the day of my baptism... why did this have to happen!?"),
                e.Write(LUKA, "Wh... What should I do!?"),
                e.Write("I haven't ever fought a monster before... in fact, I've never even seen one."),
                e.Write("However, one day I will fight the Monster Lord./If I can't face a monster of this level, how can I expect to be a Hero!?"),
                e.E.AddChoices(
                    new Choice("Fight", intoBattle()),
                    new Choice("Run", null)
                )
            );
    }

    private EventGroup intoBattle() {
        return new EventGroup(
                e.Write("I grab my sword and dash out of the house."),
                e.Write("The village where I was born and raised.../I will defend it!"),
                e.E.AddTransition()
                    .AddSound(s.steps),
                e.Write("Farmer", "Ahh! Run away!")
                    .SetBackground(b.town)
                    .AddSprite(c.villagers.tallHair),
                e.Write("Man", "Ahh! What should I do!?")
                    .DeleteSprite(c.villagers.tallHair)
                    .AddSprite(c.villagers.tallBald),
                e.Write("The village has fallen into chaos./The villagers who were working the fields are all rushing back into their homes.")
                    .DeleteSprite(c.villagers.tallBald),
                e.Write("Running against the wave of people, I head towards the village entrance./While pushing past the flood of people, I hear the booming voice of Betty, the old lady next door."),
                e.Write("Betty", "Stop, Luka!/Leave the monster to the soldiers at Ilias Temple!")
                    .AddSprite(c.villagers.betty),
                e.Write(LUKA, "It's ok, Betty... I'm a Hero!"),
                e.Write("Betty", "\"Hero\", you say? You haven't even been baptized yet!/Come back here, don't go!"),
                e.Write("It's true that I haven't received the baptism yet and am not formally a Hero.../But even so, I should be able to defeat a low level monster!"),
                e.Write("Running past Betty, I continue through the village./Finally, I reach the forest outside the village!"),
                e.E.AddTransition()
                    .AddSound(s.steps),
                e.Write("I head off from the main road into the forest./I stop running and look around the area.")
                    .DeleteSprite(c.villagers.betty)
                    .SetBackground(b.woods),
                e.Write(LUKA, "Where are you, monster?"),
                e.Write("Looking around at the eerily quiet forest, I begin to doubt rushing in with no information./Perhaps, I shouldn't have just blindly jumped into the forest..."),
                e.Write("As I look around at my surroundings, I hear a noise come from behind me./Suddenly, a monster appears!"),
                e.E.StartBattle(en.slime, new EventGroup())
            );
    }

    public void Play(params IEvent[] events) {
        StartCoroutine(PlayEvents(events));
    }

    private EventGroup Eg(params IEvent[] events) {
        return new EventGroup(events);
    }

    private IEnumerator PlayEvents(IEvent[] events) {
        foreach (IEvent e in events) {
            yield return e.Play();
        }
    }

}

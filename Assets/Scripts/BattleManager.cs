﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour {
    private static BattleManager _instance;

    [SerializeField]
    private Textbox textbox;

    [SerializeField]
    private Player player;

    [SerializeField]
    private EventManager e;

    [SerializeField]
    private Transform enemyHolder;

    [SerializeField]
    private Transform playerChoicePanel;

    [SerializeField]
    private Transform playerStatsPanel;

    [SerializeField]
    private AudioClip battleStart;

    [SerializeField]
    private AudioClip battle;

    private Enemy enemy;

    private IEvent onDefeat;

    public static BattleManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<BattleManager>();
            }
            return _instance;
        }
    }

    public void Init(Enemy enemyPrefab, EventGroup onDefeat) {
        this.textbox.SetName("Luka");
        this.player.Init(420, 1, 1, 1, 1, 2);
        this.enemy = Instantiate(enemyPrefab);
        enemy.transform.SetParent(enemyHolder);
        playerStatsPanel.gameObject.SetActive(true);
        StartCoroutine(DoBattle());
        this.onDefeat = onDefeat;
    }

    private IEnumerator DoBattle() {
        yield return DoEvents(enemy.OnBattleStart());
        SoundManager.Instance.Play(battleStart, true);
        yield return textbox.Write(enemy.name + " appears!");
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Mouse0));
        SoundManager.Instance.Loop(battle);
        while (enemy.Health.Mod > 0 && player.Health.Mod > 0) {

            Spell chosen = null;
            Action<Spell> getSpell = spell => {
                chosen = spell;
            };

            player.TakeTurn(getSpell, enemy);
            yield return new WaitUntil(() => chosen != null);
            yield return CastSpell(User.PLAYER, player, enemy, chosen);
            yield return CastSpell(User.ENEMY, player, enemy, enemy.GetSpell(player));
        }   
        yield return DoEvents(enemy.OnBattleEnd());

        if (player.Health.Mod <= 0) {
            yield return onDefeat.Play();
        }
    }

    private IEnumerator CastSpell(User user, Player player, Enemy enemy, Spell spell) {
        Cast cast = new Cast(user, spell, player, enemy);
        yield return cast.Perform();
    }

    private static IEnumerator DoEvents(IEnumerable<Event> events) {
        foreach (Event e in events) {
            yield return e.Play();
        }
    }

    private static IEnumerator DoEvents(Event e) {
        yield return e.Play();
    }
}

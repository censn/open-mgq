﻿using UnityEngine;

public class OneShotAnimation : MonoBehaviour {
    public void Destroy() {
        Destroy(this.gameObject);
    }
}
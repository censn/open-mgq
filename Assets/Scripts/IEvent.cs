﻿using System.Collections;
using System.Collections.Generic;

public interface IEvent {
    IEnumerator Play();
}
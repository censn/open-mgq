﻿using System.Collections;

public class Guard : Spell {
    public Guard() : base("Guard", 0) {
    }

    public override Calculation CalculateAmount(Unit caster, Unit target) {
        return new Calculation();
    }

    public override IEnumerator DoAnimation() {
        throw new System.NotImplementedException();
    }

    protected override IEnumerator DoEffectHelper(Unit caster, Unit target, Calculation amount) {
        throw new System.NotImplementedException();
    }
}
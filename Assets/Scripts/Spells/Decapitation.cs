﻿using System.Collections;

public class Decapitation : Spell {
    public Decapitation() : base("Demon Decapitation", 2) {

    }

    public override Calculation CalculateAmount(Unit caster, Unit target) {
        return new Calculation() { damage = 30 };
    }

    public override IEnumerator DoAnimation() {
        yield return EffectManager.Instance.PlayDecapitation();
    }

    protected override IEnumerator DoEffectHelper(Unit caster, Unit target, Calculation amount) {
        yield return target.Health.Mod -= amount.damage;
    }
}
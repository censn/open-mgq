﻿using System.Collections;

public class Struggle : Spell {

    public Struggle() : base("Struggle", 0) {
        this.IsUsableWhenBound = true;
    }

    public override Calculation CalculateAmount(Unit caster, Unit target) {
        return new Calculation() {
            isSuccessful = caster.statuses.GetStatus<Bound>().EscapeType == EscapeType.STRUGGLE
        };
    }

    public override IEnumerator DoAnimation() {
        yield break;
    }

    protected override IEnumerator DoEffectHelper(Unit caster, Unit target, Calculation amount) {
        if (amount.isSuccessful) {
            caster.statuses.RemoveStatus<Bound>();
        }
        yield break;
    }

    protected override bool IsCastableHelper(Unit caster, Unit target) {
        return caster.statuses.IsBound;
    }
}
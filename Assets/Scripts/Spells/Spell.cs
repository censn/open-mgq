﻿using System.Collections;

public abstract class Spell {
    protected static EffectManager _effect;

    public string Name { get; private set; }
    public int Cost { get; private set; }
    public bool IsUsableWhenBound;

    public string DetailedName {
        get {
            if (Cost > 0) {
                return string.Format("{0} ({1})", Name, Cost);
            }
            return Name;
        }
    }

    public EffectManager Effect {
        get {
            if (_effect == null) {
                _effect = EffectManager.Instance;
            }
            return _effect;
        }
    }

    protected Spell(string name, int cost) {
        Name = name;
        Cost = cost;
        IsUsableWhenBound = false;
    }

    protected Spell(string name) : this(name, 0) {

    }

    public abstract IEnumerator DoAnimation(); 

    public virtual string GetText(string caster, string victim, Calculation amount) {
        if (amount.damage == 0 && !amount.isSuccessful) {
            return string.Format("{0}'s {1} failed.", caster, victim);
        }
        if (amount.damage > 0) {
            return string.Format("{0}'s {1} hits {2} for {3} damage!", caster, this.Name, victim, amount.damage);
        }
        return "Huh???";
    }

    public abstract Calculation CalculateAmount(Unit caster, Unit target);

    public IEnumerator DoEffect(Unit caster, Unit target, Calculation amount) {
        caster.Skill.Mod -= Cost;
        yield return DoEffectHelper(caster, target, amount);
    }

    public bool IsCastable(Unit caster, Unit target) {
        return (!caster.statuses.IsBound || this.IsUsableWhenBound)
            && (caster.Skill.Mod >= Cost) 
            && IsCastableHelper(caster, target);
    }

    protected virtual bool IsCastableHelper(Unit caster, Unit target) {
        return true;
    }

    protected abstract IEnumerator DoEffectHelper(Unit caster, Unit target, Calculation amount);
}
﻿public abstract class SpellEffect {
    public Unit caster;
    public Unit target;

    public abstract void Perform();
}
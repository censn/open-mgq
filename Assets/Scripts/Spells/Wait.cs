﻿using System.Collections;

public class Wait : Spell {
    public Wait() : base("Wait", 0) {
    }

    public override Calculation CalculateAmount(Unit caster, Unit target) {
        return new Calculation();
    }

    public override IEnumerator DoAnimation() {
        yield break;
    }

    protected override IEnumerator DoEffectHelper(Unit caster, Unit target, Calculation amount) {
        throw new System.NotImplementedException();
    }
}
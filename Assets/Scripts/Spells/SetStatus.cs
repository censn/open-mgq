﻿using System.Collections;
using UnityEngine;

public class SetStatus<T> : Spell where T : Status {

    private T statusPrefab;

    public SetStatus(T status) : base(status.name) {
        this.statusPrefab = status;
    }

    public override Calculation CalculateAmount(Unit caster, Unit target) {
        return new Calculation();
    }

    public override IEnumerator DoAnimation() {
        yield break;
    }

    protected override IEnumerator DoEffectHelper(Unit caster, Unit target, Calculation amount) {
        if (!target.statuses.HasStatus<T>()) {
            target.statuses.AddStatus(GameObject.Instantiate(statusPrefab));
        }
        yield break;
    }
}
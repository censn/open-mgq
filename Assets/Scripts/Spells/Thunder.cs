﻿using System.Collections;

public class Thunder : Spell {
    public Thunder() : base("Thunder of Judgement") { }

    public override Calculation CalculateAmount(Unit caster, Unit target) {
        return new Calculation() { damage = 666 };
    }

    public override IEnumerator DoAnimation() {
        yield return Effect.PlayLightning();
    }

    protected override IEnumerator DoEffectHelper(Unit caster, Unit target, Calculation amount) {
        target.Health.Mod -= amount.damage;
        yield break;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cast {
    private static EventManager e = EventManager.Instance;

    public Calculation Amount;
    public Spell Spell;
    public Player Player;
    public Enemy Enemy;
    public User User;

    public Cast(User user, Spell spell, Player player, Enemy enemy) {
        this.Spell = spell;
        this.Amount = spell.CalculateAmount(player, enemy);
        this.Player = player;
        this.Enemy = enemy;
        this.User = user;
    }

    public IEnumerator Perform() {
        if (User == User.PLAYER) {
            Player.Skill.Mod -= Spell.Cost;
            yield return DoEvents(Enemy.OnBeforeHeroCast(this));
        } else {
            yield return DoEvents(Enemy.OnBeforeSelfCast(this));
        }

        yield return Spell.DoAnimation();

        // if spell does damage...
        SpriteRenderer portrait = null;
        if (User == User.PLAYER) {
            portrait = Enemy.Portrait;
        } else {
            portrait = Player.Portrait;
        }
        yield return Util.Flicker(portrait, 0.20f, 0.02f);

        if (User == User.PLAYER) {
            yield return Spell.DoEffect(Player, Enemy, Amount);
        } else {
            yield return Spell.DoEffect(Enemy, Player, Amount);
        }

        string caster = (User == User.PLAYER) ? Player.name : Enemy.name;
        string target = (User == User.PLAYER) ? Enemy.name : Player.name;

        yield return DoEvents(e.Write(Spell.GetText(caster, target, Amount)));
        if (User == User.PLAYER) {
            yield return DoEvents(Enemy.OnAfterHeroCast(this));
        } else {
            yield return DoEvents(Enemy.OnAfterSelfCast(this));
        }
    }

    private static IEnumerator DoEvents(IEnumerable<Event> events) {
        foreach (Event e in events) {
            yield return e.Play();
        }
    }

    private static IEnumerator DoEvents(Event e) {
        yield return e.Play();
    }
}

﻿using System.Collections;
using UnityEngine;

public class Attack : Spell {

    public Attack() : base("Attack", 0) {
        this.IsUsableWhenBound = true;
    }

    public override Calculation CalculateAmount(Unit caster, Unit target) {
        return new Calculation() { damage = 10 };
    }

    public override IEnumerator DoAnimation() {
        yield return EffectManager.Instance.PlayAttack();
    }

    protected override IEnumerator DoEffectHelper(Unit caster, Unit target, Calculation amount) {
        target.Health.Mod -= amount.damage;
        yield break;
    }
}

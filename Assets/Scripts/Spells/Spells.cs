﻿public static class Spells {
    public static class Hero {
        public static Attack Attack = new Attack();
        public static Guard Guard = new Guard();
        public static Struggle Struggle = new Struggle();
        public static Wait Wait = new Wait();
        public static Surrender Surrender = new Surrender();
        public static Decapitation Decapitation = new Decapitation();
    }
    public static class Enemy {
        public static TestEnemyAttack Attack = new TestEnemyAttack();
        public static Thunder Thunder = new Thunder();
    }
}
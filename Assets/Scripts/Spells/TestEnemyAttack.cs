﻿using System.Collections;

public class TestEnemyAttack : Spell {
    public TestEnemyAttack() : base("Test Enemy Attack", 0) {
    }

    public override Calculation CalculateAmount(Unit caster, Unit target) {
        return new Calculation() { damage = 10 };
    }

    public override IEnumerator DoAnimation() {
        yield return EffectManager.Instance.PlayScreenShake();
    }

    protected override IEnumerator DoEffectHelper(Unit player, Unit enemy, Calculation amount) {
        yield return player.Health.Mod -= amount.damage;
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Util {

    public static float Random(float min, float max) {
        return UnityEngine.Random.Range(min, max);
    }

    public static float Random(int min, int max) {
        return UnityEngine.Random.Range(min, max);
    }

    public static IEnumerator Lerp(float duration, Action<float> perStep) {
        float timer = 0;
        while ((timer += Time.deltaTime) < duration) {
            perStep(timer / duration);
            yield return null;
        }
        perStep(1);
    }

    public static IEnumerator Flicker(SpriteRenderer target, float duration, float maxSecondsPerFlick) {
        float timer = 0;
        while ((timer += Time.deltaTime) < duration) {
            float secondsPerFlick = Mathf.SmoothStep(0, maxSecondsPerFlick, timer / duration);
            target.enabled = !target.enabled;
            yield return new WaitForSeconds(secondsPerFlick);
            timer += secondsPerFlick;
        }
        target.enabled = true;
    }

    public static void KillAllChildren(Transform target) {
        foreach (Transform t in target.GetComponentsInChildren<Transform>()) {
            if (t != target) {
                GameObject.Destroy(t.gameObject);
            }
        }
    }

    public static void SetAlpha(Image i, float alpha) {
        Color c = i.color;
        c.a = alpha;
        i.color = c;
    }

    // assumes scales are initially 1,1,1
    public static IEnumerator Shake(float shakeIntensity, float scaleIntensity, float duration, params Transform[] targets) {
        Vector3[] originalPos = new Vector3[targets.Length];
        for (int i = 0; i < targets.Length; i++) {
            originalPos[i] = targets[i].transform.localPosition;
        }
        yield return Util.Lerp(duration, t => {
            for (int i = 0; i < targets.Length; i++) {
                Transform target = targets[i];
                Vector3 originalPosition = originalPos[i];
                Vector3 offset = new Vector3(shakeIntensity * Random(-1f, 1f), shakeIntensity * Random(-1f, 1f), 0);
                Vector3 scaleShift = new Vector3(Random(1 - scaleIntensity, 1 + scaleIntensity), Random(1 - scaleIntensity, 1 + scaleIntensity), 1);
                target.localPosition = (originalPosition + offset);
                target.localScale = scaleShift;
            }
        });
        for (int i = 0; i < targets.Length; i++) {
            targets[i].transform.localPosition = originalPos[i];
            targets[i].transform.localScale = Vector3.one;
        }
    }
}

public static class IEnumerableExt {
    /// <summary>
    /// Wraps this object instance into an IEnumerable&lt;T&gt;
    /// consisting of a single item.
    /// </summary>
    /// <typeparam name="T"> Type of the object. </typeparam>
    /// <param name="item"> The instance that will be wrapped. </param>
    /// <returns> An IEnumerable&lt;T&gt; consisting of a single item. </returns>
    public static IEnumerable<T> Yield<T>(this T item) {
        yield return item;
    }
}
﻿public struct Choice {
    public string name;
    public IEvent e;

    public Choice(string name, IEvent e) {
        this.name = name;
        this.e = e;
    }
}
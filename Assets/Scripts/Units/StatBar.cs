﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatBar : MonoBehaviour {
    [SerializeField]
    private Image filled;

    [SerializeField]
    private TextMeshProUGUI fraction;

    public bool isHideAfter;

    private Coroutine routine;

    private int numerator;
    private int denominator;

    public void SetFill(int numerator, int denominator) {
        if (routine != null) {
            StopCoroutine(routine);
        }
        this.numerator = numerator;
        this.denominator = denominator;

        if (gameObject.activeInHierarchy) {
            routine = StartCoroutine(AnimateFill(numerator, denominator));
        }
    }

    private void OnEnable() {
        routine = StartCoroutine(AnimateFill(numerator, denominator));
    }

    private IEnumerator AnimateFill(int numerator, int denominator) {
        float newFill = numerator / (float)denominator;

        if (float.IsNaN(newFill)) {
            newFill = 0;
        }

        float currentFill = filled.fillAmount;
        fraction.text = string.Format("{0}/{1}", numerator, denominator);
        yield return Util.Lerp(0.125f, t => filled.fillAmount = Mathf.SmoothStep(currentFill, newFill, t));
        if (isHideAfter) {
            yield return new WaitForSeconds(1);
            gameObject.SetActive(false);
        }
    }
}

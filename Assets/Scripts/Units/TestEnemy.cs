﻿
using System.Collections;
using UnityEngine;

public class TestEnemy : Enemy {
    [SerializeField]
    private Sprite normal;

    [SerializeField]
    private Bound bound;

    private int counter;

    public override Spell GetSpell(Player player) {
        counter++;
        if (counter % 3 == 0 && !player.statuses.HasStatus<Bound>()) {
            return new SetStatus<Bound>(bound);
        }
        return Spells.Enemy.Attack;
    }

    public void Start() {
        base.Init(normal, 100, 10, 100);
    }

    public override Event[] OnBattleEnd() {
        return new Event[] { e.Write("Battle end text") };
    }
}
﻿using System;
using UnityEngine;

public class Stat {
    private int _mod;
    private int _max;
    private StatBar _associated;

    public StatBar Associated {
        set {
            this._associated = value;
            UpdateBar();
        }
        private get {
            return _associated;
        }
    }

    public int Mod {
        get {
            return _mod;
        }
        set {
            _mod = Mathf.Clamp(value, 0, Max);
            UpdateBar();
        }
    }

    public int Max {
        get {
            return _max;
        }
        set {
            _max = Mathf.Clamp(value, 0, int.MaxValue);
            UpdateBar();
        }
    }

    public Stat(int max, StatBar associated) {
        this.Associated = associated;
        this.Max = max;
        this.Mod = max;
        UpdateBar();
    }

    public Stat(int max) : this(max, null) { }

    private void UpdateBar() {
        if (Associated != null) {
            Associated.gameObject.SetActive(true);
            Associated.SetFill(_mod, _max);
        }
    }
}
﻿using UnityEngine;

public class Ilias : Enemy {

    private void Start() {
        base.Init(s.ilias.angry, 10000, 100, 0);
    }

    public override Spell GetSpell(Player player) {
        return Spells.Enemy.Thunder;
    }
}
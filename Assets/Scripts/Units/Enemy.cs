﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : Unit {

    protected static SpriteList s;
    protected static EventManager e;

    private void Awake() {
        if (s == null) {
            s = FindObjectOfType<SpriteList>();
        }
        if (e == null) {
            e = FindObjectOfType<EventManager>();
        }
    }

    protected void Init(Sprite sprite, int maxHealth, int level, int experience) {
        this.healthBar.isHideAfter = true;
        this.Portrait.sprite = sprite;
        base.Init(maxHealth, level, experience, 0);
    }

    public virtual Event[] OnBattleStart() {
        return new Event[0];
    }

    public virtual Event[] OnBeforeSelfCast(Cast cast) {
        return new Event[0];
    }

    public virtual Event[] OnAfterSelfCast(Cast cast) {
        return new Event[0];
    }

    public virtual Event[] OnBeforeHeroCast(Cast cast) {
        return new Event[0];
    }

    public virtual Event[] OnAfterHeroCast(Cast cast) {
        return new Event[0];
    }

    public virtual Event[] OnBattleEnd() {
        return new Event[0];
    }

    public abstract Spell GetSpell(Player player);
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Unit : MonoBehaviour {
    [SerializeField]
    protected StatBar healthBar;

    [SerializeField]
    private SpriteRenderer portrait;

    [SerializeField]
    public StatusManager statuses;

    public Stat Health { get; private set; }
    public Stat Skill { get; private set; }
    public int Level { get; private set; }
    public int Experience { get; private set; }

    public SpriteRenderer Portrait {
        get {
            return portrait;
        }
    }

    protected void Init(int maxHealth, int level, int experience, int maxSkill) {
        this.Health = new Stat(maxHealth, healthBar);
        this.Skill = new Stat(maxSkill);
        this.Level = level;
        this.Experience = experience;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Unit {

    [SerializeField]
    private StatBar skillBar;

    [SerializeField]
    private Option optionPrefab;

    [SerializeField]
    private Transform optionPanel;

    private Spell[] skills = new Spell[] { Spells.Hero.Decapitation };

    public Stat Strength { get; private set; }
    public Stat Defense { get; private set; }

    public void Init(int maxHealth, int level, int experience, int maxStrength, int maxDefense, int maxSkill) {
        base.Init(maxHealth, level, experience, maxSkill);
        this.Strength = new Stat(maxStrength);
        this.Defense = new Stat(maxDefense);
        this.Skill.Associated = skillBar;
    }

    public void TakeTurn(Action<Spell> getSpell, Unit target) {
        getSpell += (hs) => Util.KillAllChildren(optionPanel);
        ShowMainPanel(getSpell, target);
    }

    private void ShowMainPanel(Action<Spell> getSpell, Unit target) {
        Util.KillAllChildren(optionPanel);
        AddOption(getSpell, Spells.Hero.Attack, target);
        AddSkillsSubpanel(getSpell, target);
        AddOption(getSpell, Spells.Hero.Struggle, target);
        //AddOption(getSpell, Spells.Hero.Guard, target);
        //AddOption(getSpell, Spells.Hero.Wait, target);
        //AddOption(getSpell, Spells.Hero.Surrender, target);
        // todo request 
    }

    private void AddSkillsSubpanel(Action<Spell> getSpell, Unit target) {
        Option option = Instantiate(optionPrefab);
        option.Init(
            optionPanel, 
            "Skill", 
            () => ShowSkills(getSpell, target),
            !statuses.IsBound);
    }

    private Option AddOption(string name, Action action, bool isEnabled) {
        Option option = Instantiate(optionPrefab);
        option.Init(
            optionPanel, 
            name, 
            action, 
            isEnabled);
        return option;
    }

    private void AddOption(Action<Spell> getSpell, Spell spell, Unit target) {
        Option option = AddOption(
            spell.DetailedName, () => {
                getSpell(spell);
                Skill.Mod -= spell.Cost;
            },
            (!statuses.IsBound || spell.IsUsableWhenBound) && spell.IsCastable(this, target)
            );
        option.transform.SetParent(optionPanel);
    }

    private void ShowSkills(Action<Spell> getSpell, Unit target) {
        Util.KillAllChildren(optionPanel);
        foreach (Spell spell in skills) {
            AddOption(getSpell, spell, target);
        }
        AddOption("Back", () => ShowMainPanel(getSpell, target), !this.statuses.IsBound);
    }

    private IEnumerator ShowSubpanel(Dictionary<string, Action> subActions) {
        yield return null;
    }
}

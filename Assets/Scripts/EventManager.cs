﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EventManager : MonoBehaviour {

    public static EventManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<EventManager>();
            }
            return _instance;
        }
    }

    private static EventManager _instance;

    [SerializeField]
    private Transform choicesParent;

    [SerializeField]
    private Textbox textbox;

    [SerializeField]
    private Image background;

    [SerializeField]
    private SpriteRenderer spritePrefab;

    [SerializeField]
    private Transform foreground;

    [SerializeField]
    private AudioSource music;

    [SerializeField]
    private Option optionPrefab;

    public Event E {
        get {
            return Write(string.Empty);
        }
    }

    public Event Write(string message) {
        return new Event(textbox.Write(message), textbox, background, spritePrefab, foreground, choicesParent, optionPrefab);
    }

    public Event Write(string name, string message) {
        return new Event(textbox.Write(message), textbox, background, spritePrefab, foreground, choicesParent, optionPrefab).SetName(name);
    }
}
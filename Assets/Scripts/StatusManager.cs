﻿using System.Linq;
using UnityEngine;

public class StatusManager : MonoBehaviour {
    [SerializeField]
    private Transform statusParent;

    [SerializeField]
    private Status prefab;

    public bool IsBound {
        get {
            return HasStatus<Bound>();
        }
    }

    public void AddStatus(Status status) {
        status.transform.SetParent(statusParent);
    }

    public bool HasStatus<T>() where T : Status {
        return statusParent.GetComponentsInChildren<Status>()
            .Any(status => status is T);
    }

    public T GetStatus<T>() where T : Status {
        foreach (Status status in statusParent.GetComponentsInChildren<Status>()) {
            if (status is T) {
                return (T) status;
            }
        }
        return null;
    }

    public void RemoveStatus<T>() where T: Status {
        foreach (Status status in statusParent.GetComponentsInChildren<Status>()) {
            if (status is T) {
                Destroy(status.gameObject);
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Status : MonoBehaviour {
    [SerializeField]
    private Image image;

    [SerializeField]
    private new TextMeshProUGUI name;

    [SerializeField]
    private TextMeshProUGUI duration;
}
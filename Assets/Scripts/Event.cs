﻿using Prime31.TransitionKit;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public struct Event : IEvent {
    private IEnumerator write;
    private Action actions;

    private Textbox text;
    private Image background;
    private SpriteRenderer spritePrefab;
    private Transform foreground;
    private Transform choicesParent;
    private Option prefab;

    public Event(IEnumerator write, 
        Textbox text, 
        Image background, 
        SpriteRenderer spritePrefab, 
        Transform foreground, 
        Transform choicesParent,
        Option prefab) {
        this.write = write;
        this.actions = () => { };
        this.text = text;
        this.background = background;
        this.spritePrefab = spritePrefab;
        this.foreground = foreground;
        this.choicesParent = choicesParent;
        this.prefab = prefab;
        SetName(string.Empty);
    }
    
    public Event AddTransition() {
        actions += () => {
            var fader = new FadeTransition();
            TransitionKit.instance.transitionWithDelegate(fader);
        };
        return this;
    }

    public Event AddChoices(params Choice[] choices) {
        Transform myChoicesParent = choicesParent;
        Textbox myTextbox = text;
        Option myPrefab = prefab;
        actions += () => {
            myTextbox.gameObject.SetActive(false);
            foreach (Choice choice in choices) {
                Option option = GameObject.Instantiate(myPrefab);
                option.Init(
                    myChoicesParent,
                    choice.name,
                    () => {
                        myTextbox.gameObject.SetActive(true);
                        Util.KillAllChildren(myChoicesParent);
                        GameManager.Instance.Play(choice.e);
                    });
            }
        };
        return this;
    }

    public Event SetMusic(AudioClip music) {
        actions += () => SoundManager.Instance.Loop(music);
        return this;
    }

    public Event AddSound(AudioClip sound) {
        actions += () => SoundManager.Instance.Play(sound);
        return this;
    }

    public IEnumerator Play() {
        actions();
        yield return write;
    }

    public Event SetName(string name) {
        Textbox text = this.text;
        actions += () => text.SetName(name);
        return this;
    }

    public Event StartBattle(Enemy enemyPrefab, EventGroup onDefeat) {
        Util.KillAllChildren(foreground);
        actions += () => {
            BattleManager.Instance.Init(enemyPrefab, onDefeat);
        };
        return this;
    }

    public Event AddSprite(Sprite spriteToAdd, bool isFadeIn = false) {
        SpriteRenderer spritePrefab = this.spritePrefab;
        Transform foreground = this.foreground;

        Textbox textbox = this.text;
        actions += (() => {
            SpriteRenderer rend = GameObject.Instantiate(spritePrefab);
            rend.sprite = spriteToAdd;
            rend.transform.SetParent(foreground);
            if (isFadeIn) {
                textbox.StartCoroutine(Util.Lerp(1f, t => {
                    Color c = rend.color;
                    c.a = t;
                    rend.color = c;
                }));
            }
        });
        return this;
    }

    public Event SetBackground(Sprite sprite) {
        Image background = this.background;
        actions += (() => background.sprite = sprite);
        return this;
    }

    public Event DeleteSprite(Sprite spriteToDestroy) {
        Transform foreground = this.foreground;
        actions += (() => {
            GameObject target = null;
            foreach (SpriteRenderer sr in foreground.GetComponentsInChildren<SpriteRenderer>()) {
                if (sr.sprite.Equals(spriteToDestroy)) {
                    target = sr.gameObject;
                    break;
                }
            }
            GameObject.Destroy(target);
        });
        return this;
    }
}